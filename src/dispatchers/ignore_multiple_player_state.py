from .base_dispatcher import BaseDispatcher

from MinecraftBridge.messages import PlayerState

import time

from MinecraftBridge.utils import Loggable

class IgnoreMultiplePlayerStateDispatcher(BaseDispatcher, Loggable):
	"""
	"""

	def __init__(self):

		BaseDispatcher.__init__(self)

		# Stores the most recent player state
		self._next_player_state = {}


	def next(self):

		# If nothing is in the list, then return nothing
		if len(self) == 0:
			return None

		# Return the item at the head of the list
		item = self.items[0]
		self.items = self.items[1:]

		# Is the item a PlayerState message?
		if isinstance(item, PlayerState):
			# Append the next PlayerState message, if it exists
			next_player_state = self._next_player_state.get(item.participant_id, None)
			if next_player_state is not None:
###				self.logger.info("%s:  Pumping backup message for %s", self, item.participant_id)
				self.items.append(next_player_state)
			self._next_player_state[item.participant_id] = None

		return item


	def add(self, item):

		# We want to only allow a single instance of a PlayerState message for
		# each participant.  If the item is a PlayerState message, then check
		# to see if there is another PlayerState message with the same 
		# participant id.  If so, do not add
		if isinstance(item, PlayerState):

###			self.logger.info("%s:  Received PlayerState message for %s at time %f", self, item.participant_id, time.time())
			
			for message in self.items:
				if isinstance(message, PlayerState) and message.participant_id == item.participant_id:
					self._next_player_state[message.participant_id] = message
					return

		# Add the item to the tail of the list (as the BaseDispatcher does)
		BaseDispatcher.add(self, item)




